#!/bin/bash

# Get the directory where this script is located
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Prompt for user input
echo -e "\e[34mEnter your Gerrit username or nothing and hit enter:\e[0m"
read GERRIT_USERNAME

# Change to the script directory
cd "$SCRIPT_DIR"

# Function to clone a repository if it doesn't exist
clone_repository() {
  local repo_url="$1"
  local repo_dir="$2"
  if [ ! -d "$repo_dir" ]; then
    git clone "$repo_url"
    (cd "$repo_dir" && mkdir -p `git rev-parse --git-dir`/hooks/ && curl -Lo `git rev-parse --git-dir`/hooks/commit-msg https://gerrit.wikimedia.org/r/tools/hooks/commit-msg; chmod +x `git rev-parse --git-dir`/hooks/commit-msg)
  else
    echo "Repository $repo_dir already exists, skipping clone."
  fi
}

if [ -z "$GERRIT_USERNAME" ]; then
  echo "Gerrit username not provided. Cloning using https without username."
  git clone https://gerrit.wikimedia.org/r/mediawiki/core.git
  cd "core/extensions"

  # Clone repositories only if they don't exist
  clone_repository https://gerrit.wikimedia.org/r/a/mediawiki/extensions/AbuseFilter AbuseFilter
  clone_repository https://gerrit.wikimedia.org/r/a/mediawiki/extensions/CampaignEvents CampaignEvents
  clone_repository https://gerrit.wikimedia.org/r/a/mediawiki/extensions/Echo Echo
  clone_repository https://gerrit.wikimedia.org/r/a/mediawiki/extensions/WikimediaCampaignEvents WikimediaCampaignEvents
  clone_repository https://gerrit.wikimedia.org/r/a/mediawiki/extensions/CheckUser CheckUser

  cd "../skins"
  git clone https://gerrit.wikimedia.org/r/mediawiki/skins/Vector
else
  echo "Gerrit username provided: $GERRIT_USERNAME, cloning with ssh"

  # Clone core repository only if it doesn't exist
  clone_repository "ssh://${GERRIT_USERNAME}@gerrit.wikimedia.org:29418/mediawiki/core" core

  cd "core/extensions"

  # Clone repositories only if they don't exist
  clone_repository "ssh://${GERRIT_USERNAME}@gerrit.wikimedia.org:29418/mediawiki/extensions/CampaignEvents" CampaignEvents
  clone_repository "ssh://${GERRIT_USERNAME}@gerrit.wikimedia.org:29418/mediawiki/extensions/WikimediaCampaignEvents" WikimediaCampaignEvents
  clone_repository "ssh://${GERRIT_USERNAME}@gerrit.wikimedia.org:29418/mediawiki/extensions/Echo" Echo
  clone_repository "ssh://${GERRIT_USERNAME}@gerrit.wikimedia.org:29418/mediawiki/extensions/AbuseFilter" AbuseFilter
  clone_repository "ssh://${GERRIT_USERNAME}@gerrit.wikimedia.org:29418/mediawiki/extensions/CheckUser" CheckUser

  cd "../skins"
  clone_repository "ssh://${GERRIT_USERNAME}@gerrit.wikimedia.org:29418/mediawiki/skins/Vector" Vector
fi

cd ../..
docker-compose down
docker-compose build
docker-compose up -d

echo "Waiting for the server to be ready..."
until curl -s http://localhost:8080 -o /dev/null; do
  echo ".";
  sleep 5
done

cp ./LocalSettings.php ./core/
docker exec -it mediawiki-service php /var/www/html/maintenance/update.php

echo -e "\e[32mConfigured, you should be able to access \e[34m\e]8;;http://localhost:8080\e\\]\e[34mhttp://localhost:8080\e[0m\e]8;;\e\\] - enjoy\e\\]"
