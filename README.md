# Installation
    Open this folder in your terminal
    Run the install.sh below to clone the needed repositories, it will ask yout gerrit username so type it like: myUserName or leave it blank and hit ENTER, in case it is blank, no problem it will download core and the needed extensions anyway
        . ./install.sh
    Then you just need to wait the first time it may take a while, it will say something like below when finished:
        Configured, you should be able to access ]http://localhost:8080] - enjoy]
    in case you want to recreate it jus run the . ./install.sh again
    In case you want to keep your mysql database uncomment the line below on the docker-compose.yaml to set the volume for the DB:
        # - ./data/mysql-data:/var/lib/mysql

# users, all users have the same password
    User: LocalUser1
    Pass: UserLocal123
    email: localuser1@wikimedia.org

# In case you add more extension run this code to create the extension needed DB
    - docker exec -it mediawiki-service php /var/www/html/maintenance/update.php

# In case you want to create a dump of your DB run
    - docker exec mediawiki-mysql-container mysqldump -u root -padmin local_mediawiki > ./backup.sql

# Users with real email:
    LocalUser3: QTE-WMF@outlook.com
    LocalUser4: QTE-WMF@hotmail.com
    LocalUser5: QTE-WMF@proton.me
    LocalUser6: QTEWMF@yahoo.com
    LocalUser7: qte@wikimedia.org

# To run unit tests you can use the command below or access the container directly
    - docker exec -it mediawiki-service bash -c "cd extensions/WikimediaCampaignEvents && php ../../tests/phpunit/phpunit.php tests/phpunit"

# To run compose fix
    docker exec -it mediawiki-service bash -c "cd extensions/WikimediaCampaignEvents && composer fix"

# Commands to create DB tables for sqlite, mysql and postgres
docker exec -it mediawiki-service bash -c "cd extensions/CampaignEvents && php ../../maintenance/generateSchemaSql.php --json db_patches/tables.json --sql db_patches/mysql/tables-generated.sql --type=mysql"

docker exec -it mediawiki-service bash -c "cd extensions/CampaignEvents && php ../../maintenance/generateSchemaSql.php --json db_patches/tables.json --sql db_patches/postgres/tables-generated.sql --type=postgres"

docker exec -it mediawiki-service bash -c "cd extensions/CampaignEvents && php ../../maintenance/generateSchemaSql.php --json db_patches/tables.json --sql db_patches/sqlite/tables-generated.sql --type=sqlite"


# Need help?
    - cmelo@wikimedia.org
