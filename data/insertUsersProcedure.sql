CREATE PROCEDURE InsertSampleUsers()
BEGIN
    DECLARE i INT DEFAULT 2;
    WHILE i <= 5000 DO
        INSERT INTO user (
            user_name, 
            user_real_name, 
            user_password, 
            user_newpassword, 
            user_newpass_time, 
            user_email, 
            user_touched, 
            user_token, 
            user_email_authenticated, 
            user_email_token, 
            user_email_token_expires, 
            user_registration, 
            user_editcount, 
            user_password_expires, 
            user_is_temp
        ) 
        VALUES (
            CONCAT('LocalUser', i), 
            CONCAT('Local User', i),
            -- UserLocal123
            ':pbkdf2:sha512:30000:64:G6/E3PuFIXcFx3uSYn206Q==:wRFvJ72cAp2cJitNfWQoaFsBC5c6FV2VMwmM2UG+REpxUDfzBCEFzVQFcEldeOOw8EGPmA0fWTd2JG1/S6YmKA==', 
            '', 
            NULL, 
            CONCAT('localuser', i, '@wikimedia.org'), 
            DATE_FORMAT(NOW(6), '%Y%m%d%H%i%s'),
            '', 
            DATE_FORMAT(NOW(6), '%Y%m%d%H%i%s'),
            '', 
            NULL, 
            DATE_FORMAT(NOW(6), '%Y%m%d%H%i%s'),
            0, 
            NULL, 
            0
        );
        SET i = i + 1;
    END WHILE;
END;