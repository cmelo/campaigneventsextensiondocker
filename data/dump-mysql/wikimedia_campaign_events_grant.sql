CREATE TABLE wikimedia_campaign_events_grant (
  wceg_id BIGINT UNSIGNED AUTO_INCREMENT NOT NULL,
  wceg_event_id BIGINT UNSIGNED NOT NULL,
  wceg_grant_id TINYBLOB NOT NULL,
  wceg_grant_agreement_at BINARY(14) NOT NULL,
  PRIMARY KEY(wceg_id)
);
