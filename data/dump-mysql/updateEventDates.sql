-- update event 1 and 2 to be in the future
UPDATE local_mediawiki.campaign_events
SET 
  event_start_local = DATE_FORMAT(NOW(6), '%Y%m%d%H%i%s'),
  event_start_utc = DATE_FORMAT(NOW(6), '%Y%m%d%H%i%s'),
  event_end_local = DATE_FORMAT(DATE_ADD(NOW(6), INTERVAL 15 DAY), '%Y%m%d%H%i%s'),
  event_end_utc = DATE_FORMAT(DATE_ADD(NOW(6), INTERVAL 15 DAY), '%Y%m%d%H%i%s')
WHERE event_id IN (1, 2);

UPDATE local_mediawiki.campaign_events
SET 
  event_start_local = DATE_FORMAT(DATE_SUB(NOW(6), INTERVAL 15 DAY), '%Y%m%d%H%i%s'),
  event_start_utc = DATE_FORMAT(DATE_SUB(NOW(6), INTERVAL 15 DAY), '%Y%m%d%H%i%s'),
  event_end_local = DATE_FORMAT(DATE_SUB(NOW(6), INTERVAL 10 DAY), '%Y%m%d%H%i%s'),
  event_end_utc = DATE_FORMAT(DATE_SUB(NOW(6), INTERVAL 10 DAY), '%Y%m%d%H%i%s')
WHERE event_id IN (3, 4);


-- DATE_FORMAT(NOW(6), '%Y%m%d%H%i%s')
-- DATE_FORMAT(DATE_ADD(NOW(6), INTERVAL 15 DAY), '%Y%m%d%H%i%s');
-- DATE_FORMAT(DATE_SUB(NOW(6), INTERVAL 15 DAY), '%Y%m%d%H%i%s');
