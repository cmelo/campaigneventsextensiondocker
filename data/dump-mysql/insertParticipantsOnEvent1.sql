-- DATE_FORMAT(NOW(6), '%Y%m%d%H%i%s')
-- DATE_FORMAT(DATE_ADD(NOW(6), INTERVAL 15 DAY), '%Y%m%d%H%i%s')
-- DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 15 DAY)), '%Y%m%d%H%i%s'))
-- Insert 4000 public users to ce_participants for event 1
INSERT INTO local_mediawiki.ce_participants
(cep_event_id, cep_user_id, cep_private, cep_registered_at, cep_unregistered_at, cep_first_answer_timestamp, cep_aggregation_timestamp)
SELECT 1, user_id, 0, DATE_FORMAT(NOW(6), '%Y%m%d%H%i%s'), NULL, DATE_FORMAT(NOW(6), '%Y%m%d%H%i%s'), NULL
FROM local_mediawiki.user WHERE user_id BETWEEN 2 AND 4001;

-- Insert 1000 private users to ce_participants for event 1
INSERT INTO local_mediawiki.ce_participants
(cep_event_id, cep_user_id, cep_private, cep_registered_at, cep_unregistered_at, cep_first_answer_timestamp, cep_aggregation_timestamp)
SELECT 1, user_id, 1, DATE_FORMAT(NOW(6), '%Y%m%d%H%i%s'), NULL, DATE_FORMAT(NOW(6), '%Y%m%d%H%i%s'), NULL
FROM local_mediawiki.user WHERE user_id BETWEEN 4002 AND 5002;
