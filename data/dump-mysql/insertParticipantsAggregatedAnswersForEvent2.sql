-- Insert aggregated answers for event 3
INSERT INTO local_mediawiki.ce_question_aggregation
(ceqag_event_id, ceqag_question_id, ceqag_answer_option, ceqag_answers_amount)
VALUES 
(3, 1, 1, 30),
(3, 1, 2, 4),
(3, 1, 3, 16),
(3, 1, 4, 50),
(3, 1, 5, 50),

(3, 2, 1, 30),
(3, 2, 2, 4),
(3, 2, 3, 16),

(3, 3, 1, 30),
(3, 3, 2, 4),
(3, 3, 3, 16),

(3, 4, 2, 4),
(3, 4, 3, 16),

(3, 5, 1, 30),
(3, 5, 2, 4);


-- Insert aggregated answers for event 4
INSERT INTO local_mediawiki.ce_question_aggregation
(ceqag_event_id, ceqag_question_id, ceqag_answer_option, ceqag_answers_amount)
VALUES 
(4, 1, 1, 3),
(4, 1, 2, 3),
(4, 1, 3, 6),

(4, 2, 1, 2),
(4, 2, 2, 3),
(4, 2, 3, 7),

(4, 3, 1, 3),
(4, 3, 2, 3),
(4, 3, 3, 6),

(2, 4, 2, 6),
(2, 4, 3, 6),

(2, 5, 1, 5),
(2, 5, 2, 7);
