-- DATE_FORMAT(NOW(6), '%Y%m%d%H%i%s')
-- DATE_FORMAT(DATE_ADD(NOW(6), INTERVAL 15 DAY), '%Y%m%d%H%i%s')
-- DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 15 DAY)), '%Y%m%d%H%i%s'))

-- Insert 400 public users to ce_participants for event 3
INSERT INTO local_mediawiki.ce_participants
(cep_event_id, cep_user_id, cep_private, cep_registered_at, cep_unregistered_at, cep_first_answer_timestamp, cep_aggregation_timestamp)
SELECT 3, user_id, 0, DATE_FORMAT(DATE_SUB(NOW(6), INTERVAL 13 DAY), '%Y%m%d%H%i%s'), NULL, DATE_FORMAT(DATE_SUB(NOW(6), INTERVAL 13 DAY), '%Y%m%d%H%i%s'), DATE_FORMAT(DATE_SUB(NOW(6), INTERVAL 15 DAY), '%Y%m%d%H%i%s')
FROM local_mediawiki.user WHERE user_id BETWEEN 2 AND 101;

-- Insert 50 private users to ce_participants for event 3
INSERT INTO local_mediawiki.ce_participants
(cep_event_id, cep_user_id, cep_private, cep_registered_at, cep_unregistered_at, cep_first_answer_timestamp, cep_aggregation_timestamp)
SELECT 3, user_id, 1, DATE_FORMAT(DATE_SUB(NOW(6), INTERVAL 14 DAY), '%Y%m%d%H%i%s'), NULL, DATE_FORMAT(DATE_SUB(NOW(6), INTERVAL 14 DAY), '%Y%m%d%H%i%s'), DATE_FORMAT(DATE_SUB(NOW(6), INTERVAL 14 DAY), '%Y%m%d%H%i%s')
FROM local_mediawiki.user WHERE user_id BETWEEN 102 AND 152;
