-- Insert 500 participant answers of question id 1 and option 1 for event 1
INSERT INTO local_mediawiki.ce_question_answers
(ceqa_event_id, ceqa_user_id, ceqa_question_id, ceqa_answer_option, ceqa_answer_text)
SELECT 1, user_id, 1, 1, null
FROM local_mediawiki.user WHERE user_id between 2 and 502;

-- Insert 500 participant answers of question id 1 and option 2 for event 1
INSERT INTO local_mediawiki.ce_question_answers
(ceqa_event_id, ceqa_user_id, ceqa_question_id, ceqa_answer_option, ceqa_answer_text)
SELECT 1, user_id, 1, 2, null
FROM local_mediawiki.user WHERE user_id between 503 and 1003;

-- Insert 500 participant answers of question id 2 and option 1 for event 1
INSERT INTO local_mediawiki.ce_question_answers
(ceqa_event_id, ceqa_user_id, ceqa_question_id, ceqa_answer_option, ceqa_answer_text)
SELECT 1, user_id, 2, 1, null
FROM local_mediawiki.user WHERE user_id between 2 and 502;

-- Insert 500 participant answers of question id 2 and option 2 for event 1
INSERT INTO local_mediawiki.ce_question_answers
(ceqa_event_id, ceqa_user_id, ceqa_question_id, ceqa_answer_option, ceqa_answer_text)
SELECT 1, user_id, 2, 2, null
FROM local_mediawiki.user WHERE user_id between 503 and 1003;

-- Insert 500 participant answers of question id 3 and option 1 for event 1
INSERT INTO local_mediawiki.ce_question_answers
(ceqa_event_id, ceqa_user_id, ceqa_question_id, ceqa_answer_option, ceqa_answer_text)
SELECT 1, user_id, 3, 1, null
FROM local_mediawiki.user WHERE user_id between 2 and 502;

-- Insert 500 participant answers of question id 3 and option 2 for event 1
INSERT INTO local_mediawiki.ce_question_answers
(ceqa_event_id, ceqa_user_id, ceqa_question_id, ceqa_answer_option, ceqa_answer_text)
SELECT 1, user_id, 3, 2, null
FROM local_mediawiki.user WHERE user_id between 503 and 1003;

-- Insert 500 participant answers of question id 4 and option 1 for event 1
INSERT INTO local_mediawiki.ce_question_answers
(ceqa_event_id, ceqa_user_id, ceqa_question_id, ceqa_answer_option, ceqa_answer_text)
SELECT 1, user_id, 4, 1, null
FROM local_mediawiki.user WHERE user_id between 2 and 502;

-- Insert 500 participant answers of question id 4 and option 2 for event 1
INSERT INTO local_mediawiki.ce_question_answers
(ceqa_event_id, ceqa_user_id, ceqa_question_id, ceqa_answer_option, ceqa_answer_text)
SELECT 1, user_id, 4, 2, null
FROM local_mediawiki.user WHERE user_id between 503 and 1003;

-- Insert 500 participant answers of question id 5 and option 1 for event 1
INSERT INTO local_mediawiki.ce_question_answers
(ceqa_event_id, ceqa_user_id, ceqa_question_id, ceqa_answer_option, ceqa_answer_text)
SELECT 1, user_id, 5, 1, 'Test Affiliation'
FROM local_mediawiki.user WHERE user_id between 2 and 502;

-- Insert 500 participant answers of question id 4 and option 2 for event 1
INSERT INTO local_mediawiki.ce_question_answers
(ceqa_event_id, ceqa_user_id, ceqa_question_id, ceqa_answer_option, ceqa_answer_text)
SELECT 1, user_id, 5, 2, null
FROM local_mediawiki.user WHERE user_id between 503 and 1003;


