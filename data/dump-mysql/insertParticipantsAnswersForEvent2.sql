-- Insert 500 participant answers of question id 1 and option 1 for event 1
INSERT INTO local_mediawiki.ce_question_answers
(ceqa_event_id, ceqa_user_id, ceqa_question_id, ceqa_answer_option, ceqa_answer_text)
SELECT 2, user_id, 1, 1, null
FROM local_mediawiki.user WHERE user_id between 2 and 50;

-- Insert 500 participant answers of question id 1 and option 2 for event 1
INSERT INTO local_mediawiki.ce_question_answers
(ceqa_event_id, ceqa_user_id, ceqa_question_id, ceqa_answer_option, ceqa_answer_text)
SELECT 2, user_id, 1, 2, null
FROM local_mediawiki.user WHERE user_id between 51 and 60;

-- Insert 500 participant answers of question id 2 and option 1 for event 1
INSERT INTO local_mediawiki.ce_question_answers
(ceqa_event_id, ceqa_user_id, ceqa_question_id, ceqa_answer_option, ceqa_answer_text)
SELECT 2, user_id, 2, 1, null
FROM local_mediawiki.user WHERE user_id between 2 and 45;

-- Insert 500 participant answers of question id 2 and option 2 for event 1
INSERT INTO local_mediawiki.ce_question_answers
(ceqa_event_id, ceqa_user_id, ceqa_question_id, ceqa_answer_option, ceqa_answer_text)
SELECT 2, user_id, 2, 2, null
FROM local_mediawiki.user WHERE user_id between 46 and 55;

-- Insert 500 participant answers of question id 3 and option 1 for event 1
INSERT INTO local_mediawiki.ce_question_answers
(ceqa_event_id, ceqa_user_id, ceqa_question_id, ceqa_answer_option, ceqa_answer_text)
SELECT 2, user_id, 3, 1, null
FROM local_mediawiki.user WHERE user_id between 2 and 10;

-- Insert 500 participant answers of question id 3 and option 2 for event 1
INSERT INTO local_mediawiki.ce_question_answers
(ceqa_event_id, ceqa_user_id, ceqa_question_id, ceqa_answer_option, ceqa_answer_text)
SELECT 2, user_id, 3, 2, null
FROM local_mediawiki.user WHERE user_id between 11 and 20;

-- Insert 500 participant answers of question id 4 and option 1 for event 1
INSERT INTO local_mediawiki.ce_question_answers
(ceqa_event_id, ceqa_user_id, ceqa_question_id, ceqa_answer_option, ceqa_answer_text)
SELECT 2, user_id, 4, 1, null
FROM local_mediawiki.user WHERE user_id between 2 and 5;

-- Insert 500 participant answers of question id 4 and option 2 for event 1
INSERT INTO local_mediawiki.ce_question_answers
(ceqa_event_id, ceqa_user_id, ceqa_question_id, ceqa_answer_option, ceqa_answer_text)
SELECT 2, user_id, 4, 2, null
FROM local_mediawiki.user WHERE user_id between 6 and 9;

-- Insert 500 participant answers of question id 5 and option 1 for event 1
INSERT INTO local_mediawiki.ce_question_answers
(ceqa_event_id, ceqa_user_id, ceqa_question_id, ceqa_answer_option, ceqa_answer_text)
SELECT 2, user_id, 5, 1, 'Test Affiliation'
FROM local_mediawiki.user WHERE user_id between 2 and 27;

-- Insert 500 participant answers of question id 4 and option 2 for event 1
INSERT INTO local_mediawiki.ce_question_answers
(ceqa_event_id, ceqa_user_id, ceqa_question_id, ceqa_answer_option, ceqa_answer_text)
SELECT 2, user_id, 5, 2, null
FROM local_mediawiki.user WHERE user_id between 28 and 35;


