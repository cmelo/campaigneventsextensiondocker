-- DATE_FORMAT(DATE_SUB(NOW(6), INTERVAL 12 DAY)
-- DATE_FORMAT(DATE_ADD(NOW(6), INTERVAL 15 DAY), '%Y%m%d%H%i%s'))
-- DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 15 DAY), '%Y%m%d%H%i%s'))

-- Insert 40 publicly registered with a confirmed email address for event 4
INSERT INTO local_mediawiki.ce_participants
(cep_event_id, cep_user_id, cep_private, cep_registered_at, cep_unregistered_at, cep_first_answer_timestamp, cep_aggregation_timestamp)
SELECT 4, user_id, 0, DATE_FORMAT(DATE_SUB(NOW(6), INTERVAL 12 DAY), '%Y%m%d%H%i%s'), NULL, DATE_FORMAT(DATE_SUB(NOW(6), INTERVAL 12 DAY), '%Y%m%d%H%i%s'), DATE_FORMAT(DATE_SUB(NOW(6), INTERVAL 12 DAY), '%Y%m%d%H%i%s')
FROM local_mediawiki.user WHERE user_id BETWEEN 2 AND 42;

-- Insert 10 publicly registered without a confirmed email address for event 4
INSERT INTO local_mediawiki.ce_participants
(cep_event_id, cep_user_id, cep_private, cep_registered_at, cep_unregistered_at, cep_first_answer_timestamp, cep_aggregation_timestamp)
SELECT 4, user_id, 0, DATE_FORMAT(DATE_SUB(NOW(6), INTERVAL 12 DAY), '%Y%m%d%H%i%s'), NULL, DATE_FORMAT(DATE_SUB(NOW(6), INTERVAL 12 DAY), '%Y%m%d%H%i%s'), DATE_FORMAT(DATE_SUB(NOW(6), INTERVAL 12 DAY), '%Y%m%d%H%i%s')
FROM local_mediawiki.user WHERE user_id BETWEEN 43 AND 53;
update local_mediawiki.user set user_email_authenticated = NULL where user_id BETWEEN 43 AND 53;

-- Insert 10 privately registered with a confirmed email address for event 4
INSERT INTO local_mediawiki.ce_participants
(cep_event_id, cep_user_id, cep_private, cep_registered_at, cep_unregistered_at, cep_first_answer_timestamp, cep_aggregation_timestamp)
SELECT 4, user_id, 1, DATE_FORMAT(DATE_SUB(NOW(6), INTERVAL 12 DAY), '%Y%m%d%H%i%s'), NULL, DATE_FORMAT(DATE_SUB(NOW(6), INTERVAL 12 DAY), '%Y%m%d%H%i%s'), DATE_FORMAT(DATE_SUB(NOW(6), INTERVAL 12 DAY), '%Y%m%d%H%i%s')
FROM local_mediawiki.user WHERE user_id BETWEEN 54 AND 64;

-- Insert 1 privately registered with a confirmed email address who then was deleted for event 4
INSERT INTO local_mediawiki.ce_participants
(cep_event_id, cep_user_id, cep_private, cep_registered_at, cep_unregistered_at, cep_first_answer_timestamp, cep_aggregation_timestamp)
SELECT 4, user_id, 1, DATE_FORMAT(DATE_SUB(NOW(6), INTERVAL 12 DAY), '%Y%m%d%H%i%s'), NULL, DATE_FORMAT(DATE_SUB(NOW(6), INTERVAL 12 DAY), '%Y%m%d%H%i%s'), DATE_FORMAT(DATE_SUB(NOW(6), INTERVAL 12 DAY), '%Y%m%d%H%i%s')
FROM local_mediawiki.user WHERE user_id = 65;
delete from local_mediawiki.user where user_id = 65;

-- Insert 1 publicly registered with a confirmed email address who then was deleted for event 4
INSERT INTO local_mediawiki.ce_participants
(cep_event_id, cep_user_id, cep_private, cep_registered_at, cep_unregistered_at, cep_first_answer_timestamp, cep_aggregation_timestamp)
SELECT 4, user_id, 0, DATE_FORMAT(DATE_SUB(NOW(6), INTERVAL 12 DAY), '%Y%m%d%H%i%s'), NULL, DATE_FORMAT(DATE_SUB(NOW(6), INTERVAL 12 DAY), '%Y%m%d%H%i%s'), DATE_FORMAT(DATE_SUB(NOW(6), INTERVAL 12 DAY), '%Y%m%d%H%i%s')
FROM local_mediawiki.user WHERE user_id = 66;
delete from local_mediawiki.user where user_id = 66;

-- Insert 1 privately registered with a confirmed email address who was globally suppressed for event 4
-- add hideuser on user_groups for this user
INSERT INTO local_mediawiki.ce_participants
(cep_event_id, cep_user_id, cep_private, cep_registered_at, cep_unregistered_at, cep_first_answer_timestamp, cep_aggregation_timestamp)
SELECT 4, user_id, 0, DATE_FORMAT(DATE_SUB(NOW(6), INTERVAL 12 DAY), '%Y%m%d%H%i%s'), NULL, DATE_FORMAT(DATE_SUB(NOW(6), INTERVAL 12 DAY), '%Y%m%d%H%i%s'), DATE_FORMAT(DATE_SUB(NOW(6), INTERVAL 12 DAY), '%Y%m%d%H%i%s')
FROM local_mediawiki.user WHERE user_id = 67;
-- TDB insert 'suppress' 

-- Insert 1 publicly registered with a confirmed email address who was globally suppressed for event 4
-- add hideuser on user_groups for this user
INSERT INTO local_mediawiki.ce_participants
(cep_event_id, cep_user_id, cep_private, cep_registered_at, cep_unregistered_at, cep_first_answer_timestamp, cep_aggregation_timestamp)
SELECT 4, user_id, 0, DATE_FORMAT(DATE_SUB(NOW(6), INTERVAL 12 DAY), '%Y%m%d%H%i%s'), NULL, DATE_FORMAT(DATE_SUB(NOW(6), INTERVAL 12 DAY), '%Y%m%d%H%i%s'), DATE_FORMAT(DATE_SUB(NOW(6), INTERVAL 12 DAY), '%Y%m%d%H%i%s')
FROM local_mediawiki.user WHERE user_id = 68;
-- TDB insert  'suppress'

-- Insert 1 privately registered without a confirmed email address who then was deleted for event 4
INSERT INTO local_mediawiki.ce_participants
(cep_event_id, cep_user_id, cep_private, cep_registered_at, cep_unregistered_at, cep_first_answer_timestamp, cep_aggregation_timestamp)
SELECT 4, user_id, 0, DATE_FORMAT(DATE_SUB(NOW(6), INTERVAL 12 DAY), '%Y%m%d%H%i%s'), NULL, DATE_FORMAT(DATE_SUB(NOW(6), INTERVAL 12 DAY), '%Y%m%d%H%i%s'), DATE_FORMAT(DATE_SUB(NOW(6), INTERVAL 12 DAY), '%Y%m%d%H%i%s')
FROM local_mediawiki.user WHERE user_id = 69;
delete from local_mediawiki.user where user_id = 69;

-- Insert 1 publicly registered without a confirmed email address who then was deleted for event 4
INSERT INTO local_mediawiki.ce_participants
(cep_event_id, cep_user_id, cep_private, cep_registered_at, cep_unregistered_at, cep_first_answer_timestamp, cep_aggregation_timestamp)
SELECT 4, user_id, 0, DATE_FORMAT(DATE_SUB(NOW(6), INTERVAL 12 DAY), '%Y%m%d%H%i%s'), NULL, DATE_FORMAT(DATE_SUB(NOW(6), INTERVAL 12 DAY), '%Y%m%d%H%i%s'), DATE_FORMAT(DATE_SUB(NOW(6), INTERVAL 12 DAY), '%Y%m%d%H%i%s')
FROM local_mediawiki.user WHERE user_id = 70;
delete from local_mediawiki.user where user_id = 70;

